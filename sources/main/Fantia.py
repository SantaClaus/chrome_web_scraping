import sys
import time
sys.path.append('../util')
import Config
import random
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Fantia:
    def GetUrls(driver, urls):
        elements = driver.find_elements_by_css_selector('.user-avatar')
        for element in elements:
            urls.append(element.get_attribute('href'))

    if __name__ == '__main__':
        driver = webdriver.Chrome('../config/chromedriver')
        wait = WebDriverWait(driver, 20)
        config = Config.Config('fantia')
        urls = []

        # ログイン
        driver.get("https://fantia.jp/auth/login")
        wait.until(EC.presence_of_element_located((By.XPATH, '//input[@id="input-mail"]')))
        elem = driver.find_element_by_xpath('//input[@id="input-mail"]')
        elem.clear()
        elem.send_keys(config.GetValue('id'))
        wait.until(EC.presence_of_element_located((By.XPATH, '//input[@id="input-pass"]')))
        elem = driver.find_element_by_xpath('//input[@id="input-pass"]')
        elem.clear()
        elem.send_keys(config.GetValue('pass'))
        elem.send_keys(Keys.RETURN)

        # プラン一覧画面へ
        wait.until(EC.presence_of_element_located((By.XPATH, '//a[@href="/mypage/users/plans"]')))
        driver.find_elements_by_xpath('//a[@href="/mypage/users/plans"]')[1].click()
        wait.until(EC.presence_of_element_located((By.XPATH, '//span[text()="無料プラン"]')))

        # 有料プラン一覧
        while True:
            GetUrls(driver, urls)
            # 11プラン以上か？（プラン一覧に2ページ目以降があるか？）
            if len(driver.find_elements_by_xpath('//a[@rel="next"]')) > 0:
                driver.find_element_by_xpath('//a[@rel="next"]').click()
            else:
                break

        # 無料プラン一覧
        driver.find_element_by_xpath('//span[text()="無料プラン"]').click()
        time.sleep(3)
        while True:
            GetUrls(driver, urls)
            if len(driver.find_elements_by_xpath('//a[@rel="next"]')) > 0:
                driver.find_element_by_xpath('//a[@rel="next"]').click()
                time.sleep(3)
            else:
                break
        random.shuffle(urls)
        for url in urls:
            driver.get(url)
            wait.until(EC.presence_of_element_located((By.XPATH, '//*[text()="支援ポイントを貯める"]')))
            driver.find_element_by_xpath('//*[text()="支援ポイントを貯める"]').click()
            try:
                # 早すぎてDOMにあっても画面表示されない要素クリックしようとして落ちる？ことがあるので保険のsleep入れてる
                wait.until(EC.presence_of_element_located((By.XPATH, '//a[@ng-click="ctrl.shareTwitter()"]')))
                time.sleep(3)
                driver.find_element_by_xpath('//a[@ng-click="ctrl.shareTwitter()"]').click()
                wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), " 応援ツイートで")]')))
                # ログインボーナス
                if url == config.GetValue('bonusPoint'):
                    driver.find_element_by_xpath('//a[@ng-click="ctrl.giveLoginBonus()"]').click()
                    wait.until(EC.presence_of_element_located((By.XPATH, '//span[contains(text(), " ログインボーナスで")]')))
            except:
                print('fail : ' + str(url))
            else:
                print('sucess : ' + str(url))
        handle_array = driver.window_handles
        for active in handle_array:
            driver.switch_to.window(active)
            driver.close()