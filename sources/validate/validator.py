import cerberus

schema = {
    'int' : {
        'required': True,
        'type': 'integer',
    }
}

def valid(valid_type, text):
    return cerberus.Validator(schema).validate({valid_type: text})

if __name__ == '__main__':
    print(valid('int','10'))
    print(valid('int',10))