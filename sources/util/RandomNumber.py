import sys
sys.path.append('../validate')
import validator
import random

class RandomNumber:
        def __init__(self):
            self.__min = 1
            self.__max = 10

        def __init__(self,a,b):
            if validator.valid('int',a) and validator.valid('int',b):
                if a < b:
                    self.__min = a
                    self.__max = b
            else:
                    self.__min = b
                    self.__max = a

        def GetRandomNumberByNumericBetween(self):
            return random.randint(self.__min, self.__max)

if __name__ == '__main__':
    pass