from selenium import webdriver
from selenium.webdriver.common.keys import keys

class SnapShot:
    def __init__(self, path, name):
        self.__counter = 1
        self.__path = path
        self.__name = name

    def GetSnap(self, driver):
        driver.save_screenshot(str(self.__path) + str(self.__name) + str(self.__counter) + '.png')
        self.__counter += 1

    def GetFullSnap(self, driver):
        dh = driver.execute_script("return document.body.scrollHeight;")
        h = driver.execute_script("return document.body.clientHeight;")
        cnt = dh // h
        keisanValue = h - 20
        for i in range(cnt + 1):
            driver.save_screenshot('fullScreenShot' + str(i+1) + '-' + str(cnt+1) + '.png') # ファイル名についてそのうち直す
            driver.execute_script("window.scrollTo(0, " + str(keisanValue) + ");")
            keisanValue+=h

if __name__ == '__main__':
    pass
