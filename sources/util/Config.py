
import json

class Config:
    def __init__(self, key):
        with open('../config/config.json', 'r') as fr:
            jsonData = json.load(fr)
            self.__data = jsonData[key]

    def GetValue(self, key):
        return self.__data[key]

    def Set(self, key, value):
        self.__data[key] = value

if __name__ == '__main__':
    pass