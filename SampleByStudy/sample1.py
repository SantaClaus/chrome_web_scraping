from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# ブラウザ毎にドライバー読み込む
driver = webdriver.Chrome("./webdriver/chromedriver.exe")

# アクセス先のURLの設定
driver.get("https://www.google.co.jp/")

# アクセス先URLのtitleタグに含まれる単語を見るアサーション
# assert "Google" in driver.title

# find_element_by_nameメソッド：引数のname属性が付いたタグを探してくれる。
# 下記はGoogleの検索窓のname属性を指定
elem = driver.find_element_by_name("q")

# キー（キーボードからの入力）を送信して検索する
# 念のため一度クリアすること（公式の推奨）
elem.clear()
# 検索
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)
# 1ページ戻る
driver.back()
# 1ページ進む
driver.forward()

# 結果に含まれる情報を確かめたいときはアサーションを作る
# assert "not found" not in driver.page_source

# 開いたブラウザウィンドウを閉じる
driver.close()



