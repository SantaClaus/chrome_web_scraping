from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

driver = webdriver.Chrome("./webdriver/chromedriver.exe")
driver.get("file:///C:/git/chrome_web_scraping/html/sample.html")

# sample3.pyより効率的なSELECT要素の操作
select = Select(driver.find_element_by_name('language'))
# 並び順から選択する。disableは無視してindexを振る模様。
select.select_by_index("2")
# 項目のテキストから選択する
select.select_by_visible_text("中国語")
# 項目のvalueから選択する
select.select_by_value("fr")
# ボタンクリック
driver.find_element_by_id("submit").click()
# フォーム内の要素を呼び出したときに、自動でDOM検索してsubmit
# element.submit()
