from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains

driver = webdriver.Chrome("./webdriver/chromedriver.exe")
driver.get("file:///C:/git/chrome_web_scraping/html/sample.html")

# ドラッグ&ドロップ

element = driver.find_element_by_id("text")
target = driver.find_element_by_id("text2")

action_chains = ActionChains(driver)
action_chains.drag_and_drop(element, target).perform()