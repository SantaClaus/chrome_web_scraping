from selenium import webdriver
from selenium.webdriver.common.keys import Keys
# 目的のHTML要素を見つける
# 例：<input type="text" name="passwd" id="passwd">
# element = driver.find_element_by_id("passwd-id")
# element = driver.find_element_by_name("passwd")
# element = driver.find_element_by_xpath("//input[@id='passwd-id']") クエリ一致は最初の要素だけ帰ってくる。

driver = webdriver.Chrome("./webdriver/chromedriver.exe")
driver.get("file:///C:/git/chrome_web_scraping/html/sample.html")

# セレクト要素の操作
element = driver.find_element_by_xpath("//select[@name='language']")
all_options = element.find_elements_by_tag_name("option")
for option in all_options:
    print("Value is: %s" % option.get_attribute("value"))
    option.click()