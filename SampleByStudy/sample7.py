from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome('/usr/local/bin/chromedriver')
driver.get("https://google.com")


# 下記はGoogleの検索窓のname属性を指定
elem = driver.find_element_by_name("q")

# キー（キーボードからの入力）を送信して検索する
# 念のため一度クリアすること（公式の推奨）
elem.clear()
# 検索
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)
# 1ページ戻る
driver.back()

# 1ページ進む
#driver.forward()

driver.save_screenshot("../test.png")
driver.close()