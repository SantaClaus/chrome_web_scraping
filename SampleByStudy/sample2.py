import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# pythonのunittestパッケージを使ってテストしてみる。
# unittest→junitのpythonバージョン的なやつ
class SearchTestExample(unittest.TestCase):
    # 必ず最初に呼ばれる
    def setUp(self):
        self.driver = webdriver.Chrome('/usr/local/bin/chromedriver')


    # テストケースメソッドは、必ずtestで始まるメソッド名であること
    def test_search_by_google(self):
        driver = self.driver
        driver.get("https:www.google.com")
        # self.assertIn("Google", driver.title)
        elem = driver.find_element_by_name("q")
        elem.send_keys("golira")
        elem.send_keys(Keys.RETURN)

    # 全てのテスト終了後に呼ばれる
    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()