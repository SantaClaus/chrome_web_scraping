# 概要

seleniumでブラウザ自動操作の勉強の記録

詳しくは[Wiki](https://gitlab.com/SantaClaus/chrome_web_scraping/wikis/1.%E3%81%AF%E3%81%98%E3%82%81%E3%81%AB)を参照

Selenium非公式日本語ドキュメント

[https://kurozumi.github.io/selenium-python/index.html](https://kurozumi.github.io/selenium-python/index.html)

## 設定値について

`goodByeManual`直下に`config/config.json`を作成してください。

今のところローカルで自分で守れ状態なので、セキュアな情報を載せる場合は特に注意してください。

```json
{
    "key1": {
        "key1.1": "value1.1"
        ,"key1.2": "value1.2"
        ,"key1.3": "value1.3"
    }
    ,"key2": {
        "key2.2": "value1.1"
        ,"key2.2": "value2.2"
        ,"key2.3": "value2.3"
    }
}
```

↓は使い方サンプル。迷ったら`util/Config.py`を見てください。

```python
import sys
sys.path.append('../util')
import Config

class Sample:
    if __name__ == '__main__':
        config = Config.Config('key1')
        print(config.GetValue('key1.1'))
```

## Webドライバについて

Webドライバとブラウザのバージョンが異なるとエラーなので、適宜ドライバも更新してください。

### ダウンロード先

https://selenium.dev/documentation/en/webdriver/driver_requirements/

#### chrome

https://chromedriver.storage.googleapis.com/index.html

#### firefox

https://github.com/mozilla/geckodriver/releases

#### Edge

https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/

## 使えるメ◯ガキ

### バニラjsでXPathのチェック

↓を対象ページ開きながら開発者コンソールで打ってみて、取得できればオッケー

```javascript
document.evaluate('ここにXPath', document,null,XPathResult.FIRST_ORDERED_NODE_TYPE,null)
```

### 普段使いプロファイル食わせて毎回ログインを回避(Chrome)

↓は(Config.json使用を前提とした）サンプル。オプションで設定してあげる

```Python
options = webdriver.ChromeOptions()
options.add_argument('--user-data-dir=' + quote(config.GetValue('chromeProfilePath')))
options.add_argument('--profile-directory=Default')

driver = webdriver.Chrome(executable_path='../confing/chromedriver', options=options)
driver.get('URL')
```

既に当該プロファイルでブラウザ起動中（つまり先にChrome起動した状態でselenium動かすと）エラーになるから注意。

「じゃあ普段使いのプロファイルをコピーして食わせればいいじゃん」←理論的にはそうじゃが使い方誤ると戻すのめんどいことに。容量用法に注意。

何か事情がなきゃプロファイル食わせないで毎回ログインさせてあげた方が楽。

例えば、新規アクセス認定食らうと毎回本人認証のSMS宛のコード入力が必要で、他のログイン手段が本当に一切ないとか

### iframeとかいう敵の問題

「あれ、画面には要素があるのにseleniumが落ちる...コンソールで見ても確かにある..」

ひょっとしてその要素はiframeの中にいませんか？？？？

その場合はiframe内に潜る必要がある。

↓サンプル

```Python
# 略（ドライバの初期設定とか）
# 画面操作で対象要素がiframeに出てくるとします。
driver.find_element_by_xpath('//span[text()="おこめ"]').click()

# iframeに「おるか？」「おらんのか？」「おるな！」「なんやおるやんけ！」したらダイブします。中で動いたら最後は外に出します。
wait.until(EC.presence_of_element_located((By.XPATH, '//iframe[@title="きりたんぽ"]')))
driver.switch_to_frame(driver.find_element_by_xpath('//iframe[@title="きりたんぽ"]'))
# なんか操作
driver.switch_to.default_content()
```

iframeがいっぱいあるとこで動かすと、ちょっとだるい。

## aタグ押下後に新しいウィンドウ/タブで操作したい問題

適当なリンク開かせて、その先で操作したいけどtarget="_blank"だったとき。

seleniumが操作対象と認識している画面は自動で切り替わらないので明示的に書いてあげないと操作できません。

↓サンプル

```Python
# 略（ドライバの初期設定とか）
# なんでもいいから新しく開く。
driver.find_element_by_xpath('//a[@target="_blank"]').click()

# 操作対象画面を切り替えて、終わったら閉じて、操作対象画面を設定しなおす。
driver.switch_to.window(driver.window_handles[1])
# なんか操作
driver.close() # 閉じなくても動作は問題ないが、いらない画面なら閉じとこ
driver.switch_to.window(driver.window_handles[0])
```
